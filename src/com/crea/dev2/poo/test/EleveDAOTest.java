package com.crea.dev2.poo.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.crea.dev2.poo.beans.Eleve;
import com.crea.dev2.poo.dao.EleveDAO;

public class EleveDAOTest {
	@Before
	public void testAddListeEleveToTest() {
		int insert = 1;
		assertEquals(insert,EleveDAO.addEleve("AGUE001", "AGUE MAX", 40, "18 Rue Labat 75018 Paris"));
		assertEquals(insert,EleveDAO.addEleve("AGUE002", "AGUE MAX", 42, "19 Rue Le Monde Paris"));
		assertEquals(insert,EleveDAO.addEleve("KAMTO005", "KAMTO Diogene", 50, "54 Rue des Ebisoires 78300 Poissy"));
		assertEquals(insert,EleveDAO.addEleve("LAURENCY004", "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris"));
		assertEquals(insert,EleveDAO.addEleve("TABIS003", "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris"));
		assertEquals(insert,EleveDAO.addEleve("TAHAE002", "TAHAR RIDENE", 30, "12 Rue des Chantiers 78000 Versailles")); 
	}
	@After
	public void testDeleteListeEleveToTest() {
		int delete = 1;
		assertEquals(delete,EleveDAO.deleteEleveByNum("AGUE001"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("AGUE002"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("KAMTO005"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("LAURENCY004"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("TABIS003"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("TAHAE002")); 
		System.out.println("Eleve Supprim� ou pas : "+delete);
	}
	@Test
	public void testAddEleve() {
		int insert = 1;
		int delete = 1;
		assertEquals(insert,EleveDAO.addEleve("LILY001", "LILIANA AFONSO", 22, "6 rue de Paris 75014 Paris"));
		assertNotEquals(insert,EleveDAO.addEleve("AGUE001", "AGUE MAX", 40, "18 Rue Labat 75018 Paris"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("LILY001"));
	}
	@Test
	public void testDeleteEleveByNum() {
		int insert = 1;
		int delete = 1;
		assertEquals(insert,EleveDAO.addEleve("OPO998", "OPO SANG", 23, "6 rue de YHAN China"));
		assertEquals(delete,EleveDAO.deleteEleveByNum("OPO998"));
	}
	
	@Test
	public void testGetEleveByNum() throws SQLException {
		 Eleve e_ref= new Eleve("AGUE001", 0, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");//"AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris"
		 Eleve e_res = new Eleve();
		 e_res = EleveDAO.getEleveByNum("AGUE001");	
		 
		 assertEquals(e_ref.getAdresse(),e_res.getAdresse());
		 assertEquals(e_ref.getAge(),e_res.getAge());
		 assertEquals(e_ref.getNo(),e_res.getNo());
		 assertEquals(e_ref.getNom(),e_res.getNom());
		 assertEquals(e_ref.getNum(),e_res.getNum());
	}
	@Test
	public void testGetEleveByNom() throws SQLException {		 
	     ArrayList<Eleve> listEleve = new ArrayList<Eleve>();	     
		 Eleve e1 = new Eleve("AGUE001", 0, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");
		 Eleve e2 = new Eleve("AGUE002", 0, "AGUE MAX", 42, "19 Rue Le Monde Paris");

		 listEleve.add(e1);
		 listEleve.add(e2);
		 
		 for( int i =0; i < listEleve.size(); i++ )
		 {  
			 System.out.println("Taille Liste : "+EleveDAO.getEleveByNom("AGUE MAX").size());
			 assertEquals(listEleve.get(i).getAdresse(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getAdresse());
			 assertEquals(listEleve.get(i).getNo(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getNo());
			 assertEquals(listEleve.get(i).getNom(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getNom());
			 assertEquals(listEleve.get(i).getNum(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getNum());
			 assertEquals(listEleve.get(i).getAge(),EleveDAO.getEleveByNom("AGUE MAX").get(i).getAge());
		 }
	}
	@Test
	public void testGetAllEleve() throws SQLException {
		 ArrayList<Eleve> listEleve = new ArrayList<Eleve>();
		 
		 Eleve e1 = new Eleve("AGUE001", 0, "AGUE MAX", 40, "18 Rue Labat 75018 Paris");//"AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris"
		 Eleve e2 = new Eleve("AGUE002", 0, "AGUE MAX", 42, "19 Rue Le Monde Paris");//"AGUE001", 1, "AGUE MAX", 40, "18 Rue Labat 75018 Paris"
		 Eleve e3 = new Eleve("KAMTO005", 0, "KAMTO Diogene", 50, "54 Rue des Ebisoires 78300 Poissy");
		 Eleve e4 = new Eleve("LAURENCY004", 0, "LAURENCY Patrick", 52, "79 Rue des Poules 75015 Paris");
		 Eleve e5 = new Eleve("TABIS003",0, "Ghislaine TABIS", 30, "12 Rue du louvre 75013 Paris");
		 Eleve e6 = new Eleve("TAHAE002", 0, "TAHAR RIDENE", 30, "12 Rue des Chantiers 78000 Versailles");
	
		 listEleve.add(e1);
		 listEleve.add(e2);
		 listEleve.add(e3);
		 listEleve.add(e4);
		 listEleve.add(e5);		
		 listEleve.add(e6);		
		 
		 for( int i =0; i < listEleve.size(); i++ )
		 {  
			 assertEquals(listEleve.get(i).getAdresse(),EleveDAO.getAllEleve().get(i).getAdresse());
			 assertEquals(listEleve.get(i).getNo(),EleveDAO.getAllEleve().get(i).getNo());
			 assertEquals(listEleve.get(i).getNom(),EleveDAO.getAllEleve().get(i).getNom());
			 assertEquals(listEleve.get(i).getNum(),EleveDAO.getAllEleve().get(i).getNum());
			 assertEquals(listEleve.get(i).getAge(),EleveDAO.getAllEleve().get(i).getAge());
		 }
		
	}
}
